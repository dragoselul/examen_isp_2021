package ex2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Window extends JFrame implements ActionListener {
    private JButton button1;
    private JButton button2;
    private JTextField textField1;
    private JTextField textField2;

    public Window() {
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setLayout(new GridLayout(1, 2));
        this.setSize(200, 60);

        button1 = new JButton("Press 1");
        button1.addActionListener(this);
        this.add(button1);

        textField1.setText("Imi place Java");
        textField2.setText("");
        this.setVisible(true);
    }
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        if (actionEvent.getSource().equals(button1)) {
            System.out.println("do something");
        } else if (actionEvent.getSource().equals(button2)) {
            System.out.println("do something else");
        }

        System.out.println(actionEvent.getActionCommand());
    }
}
