package ex2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

public class App extends JFrame implements ActionListener {
    private JButton button1;
    private JTextField textField1;
    private JTextArea textArea1;

    public App() {
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setLayout(new GridLayout(1, 2));
        this.setSize(400, 120);



        button1 = new JButton("Press 1");
        textField1 = new JTextField("Imi place Java");
        textArea1 = new JTextArea("14");

        button1.addActionListener(this);
        this.add(button1);
        this.add(textField1);
        this.add(textArea1);

        textField1.setText("Imi place Java");
        this.setVisible(true);
        textField1.setVisible(true);

    }
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        if (actionEvent.getSource().equals(button1)) {
            System.out.println("do something");
        }

        System.out.println(actionEvent.getActionCommand());
    }

    public static void main(String[] args) {
        new App();

    }
}
